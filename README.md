# Projet

Ce projet a pour but de réaliser une intégration continue via Jenkins, d'une application PHP.

# Cloner l'application

```sh
git clone https://gitlab.imt-atlantique.fr/f21munie/project_jenkins_php.git
cd project_jenkins_php
```

# Lancement manuel

## Générer l'image de l'application

```sh
cd project_jenkins_php
docker build -t checktva src/
```

## Exécuter l'application

```sh
cd project_jenkins_php
```
- cas où il y a les trois arguments :
```sh
docker run --rm -ti checktva php checktva.php product food 2
```
>  OK TVA calculated
- cas où il manque un argument :
```sh
docker run --rm -ti checktva php checktva.php product food 
```
> Usage: php checktva.php <NAME> <TYPE> <PRICE>
- cas où il y a une erreur de type :
```sh
docker run --rm -ti checktva php checktva.php product food l 
```
> KO TVA not calculated Unsupported operand types: string * float

## Effectuer les tests manuellement

```sh
cd project_jenkins_php
docker build -t phpunit build/docker/phpunit/
docker run --rm -ti -v "$PWD":/app phpunit make
```

# Intégration continue sur Jenkins

## Lancer les services jenkins / regitry / ui

```sh
cd project_jenkins_php
docker-compose up
```
## Installation de Jenkins 

- Allez sur votre navigateur à l'adresse : 
```sh
http://localhost:8080
```
- Pour débloquer Jenkins : 
    - récupérer le mot de passe généré après le lancement du docker-compose 
    - le coller dans la zone "Mot de passe administrateur" de l'application

- Personnaliser Jenkins :
    - sélectionner "Installer les plugins suggérés"

- Créer le 1er utilisateur administrateur
    - renseigner les champs demandés

- Configuration de l'instance :
    - valider

## Installer les plugins du projet

- Dans le tableau de bord, allez dans "Administrer Jenkins"

- Sélectionner "Gestion des plugins"

- Sélectionner "Disponibles"

- Sélectionner les plugins suivant :
    - Docker : L'objectif de ce plugin Docker est de pouvoir utiliser un hôte Docker pour provisionner dynamiquement un conteneur Docker en tant que nœud d'agent Jenkins,
    - Docker API : Ce plugin expose l'API docker-java aux plugins Jenkins. 
    - Docker Slave : Il permet d’utiliser des conteneurs pour configurer les agents de build, sans aucune contrainte sur les images que l’on peut utiliser.
    - Docker pipeline : Plugin Jenkins qui permet de créer, tester et utiliser des images Docker à partir de projets Jenkins Pipeline.
    - Warnings Next Generation : Le plug-in Warnings Next Generation collecte les avertissements du compilateur ou les problèmes signalés par les outils d'analyse statique et visualise les résultats.
    - xUnit : Enregistre les tests xUnit et marque la construction comme instable ou échouer en fonction des valeurs de seuil.
    - Clover : Ce plugin permet de capturer des rapports de couverture de code à partir d'OpenClover. Jenkins générera et suivra la couverture du code dans le temps.
    - Blue ocean : Il gère la visualisation des pipelines pour une compréhension rapide et intuitive de l'état du pipeline logiciel. C’est un éditeur de pipeline qui rend leur automatisation accessible en guidant l'utilisateur à travers un processus intuitif et visuel pour créer un pipeline.

- Sélectionner "Download now and install after restart"

- Cocher la case "Redémarrer Jenkins quand l'installation est terminée et qu'aucun job n'est en cours"

- Après téléchargement, relancer le docker-compose et actualiser la page web

- S'identifier

## Créer un projet

- Sélectionner "Tableau de bord"

- Sélectionner "Nouveau item"

- Saisissez un nom de projet (ex: Project_jenkins_php)

- Sélectionner "Pipeline"

- Valider

## Lancer le pipeline

- Dans le projet, sélectionner "Pipeline"
    - dans "Définition", choisir "Pipeline script from SCM"
    - dans "SCM", choisir "Git"
    - dans "Repository URL", coller le lien du dépôt GitLab (https://gitlab.imt-atlantique.fr/f21munie/project_jenkins_php.git)
    - sélectionner "Apply" puis "Sauver"

- Sélectionner "Lancer un build"
    - le pipeline va se lancer et apparaître dans "Historique des builds"
    - vous pouvez le sélectionner pour afficher ses résultats
    - vous pouvez visualiser le pipeline graphiquement en sélectionnant "Open Blue Ocean" puis le pipeline exécuté

## Accès à la registry

- Accéder à la registry
```sh
http://localhost:8082/
```

- Sélectionner "checktva"

## Utiliser l'image de livraison

```sh
docker pull localhost:8082/checktva:<TAG>
```
> ex : docker pull localhost:8082/checktva:1

> 1: Pulling from checktva
> Digest: sha256:54e1fce771c2305dce96c1a1aaa7f4ac96f1c4e0e4bed993098541890ad69a71
> Status: Downloaded newer image for localhost:8082/checktva:1
> localhost:8082/checktva:1




