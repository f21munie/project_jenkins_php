pipeline {
    environment {
        QUALITY_GATE = '10'
        CODE_COVERAGE = '50'
        QUALITY_DOCKER = '5'
    }

    agent any
    stages {
        stage ('checkout') {
            agent any
            steps {
                echo "Phase 1 : Checkout"
                checkout scmGit(branches: [[name: '*/master']], extensions: [], userRemoteConfigs: [[url: 'https://gitlab.imt-atlantique.fr/f21munie/project_jenkins_php.git']])
            }
        }
        
        stage('tests') {
            parallel {
                stage('hadolint') {
                    // Phase 2 : Agent Dockerfile image hadolint
                    agent {
                        docker {
                            image 'docker.io/hadolint/hadolint:v2.12.0-alpine'
                        }
                    }
                    steps {
                        echo 'Phase 3 : Création hadolint.json'
                        sh 'touch hadolin.json'
                        echo 'Phase 4 : Exécution hadolint sur les deux Dockerfiles'
                        sh 'find ./ -iname "Dockerfile" | xargs hadolint -f json | tee -a hadolint.json'
                        recordIssues qualityGates: [[threshold: QUALITY_DOCKER, type: 'TOTAL', unstable: false]], tools: [hadoLint(pattern: 'hadolint.json')]
                        sh 'rm hadolint.json'
                    }
                }    

                stage('tests statiques') {
                    // "Phase 5 : Agent Dockerfile intégration"
                    agent {
                        dockerfile {
                            dir 'build/docker/phpunit'
                            filename 'Dockerfile'
                        }
                    }
                    steps {
                        echo "Phase 6 : Préparation de l'environnement"
                        sh 'ls -l'
                        sh 'make clean'
                        sh 'ls -l'
                        echo "Phase 7: Réalisation des tests statiques"
                        sh 'make tests_statiques'
                        echo "Phase 8: Publication des erreurs checkstyle/pmd avec un seuil a 10"
                        recordIssues qualityGates: [[threshold: QUALITY_GATE, type: 'TOTAL', unstable: false]], tools: [pmdParser(pattern: 'build/logs/pmd.xml'), phpCodeSniffer(pattern: 'build/logs/checkstyle.xml')]            
                    }
                }

                stage('Tests unitaires') {
                    agent {
                        dockerfile {
                            dir 'build/docker/phpunit'
                            filename 'Dockerfile'
                        }
                    }
                    steps {
                        echo "Phase 9 : Préparation de l'environnement"
                        sh 'ls -l'
                        sh 'make clean'
                        sh 'ls -l'
                        echo "Phase 10 : Réalisation des tests unitaires"
                        sh 'make tests_unitaires'
                        echo "Phase 11 : Publication du résultat des tests junit"
                        xunit checksName: '', tools: [PHPUnit(excludesPattern: '', pattern: 'build/logs/junit.xml', stopProcessingIfError: true)]
                        echo "Phase 12 : Publication de la couverture de code"
                        clover(cloverReportDir: 'build/coverage', cloverReportFileName: 'coverage.xml',
                            healthyTarget: [methodCoverage: 70, conditionalCoverage: 80, statementCoverage: CODE_COVERAGE],
                            unhealthyTarget: [methodCoverage: 50, conditionalCoverage: 50, statementCoverage: 50],
                            failingTarget: [methodCoverage: 0, conditionalCoverage: 0, statementCoverage: 0])
                    }
                }
            }
        }

        stage('deploy') {
            agent any
            steps {
                echo "Phase 13 : Script de construction l'image numérotée"
                echo "Phase 14 : Script de publication de l'imagesur le registre privé"
                script {
                    docker.withRegistry('http://localhost:5000') {
                        docker.build("checktva:${env.BUILD_ID}", "./src").push()
                    }    
                }
            }
        }
    }
    post {
        always {
            echo "Phase 15 : Nettoyage de l'espace de travail"
            cleanWs()
        }
    }

}