<?php
require "tva.php";

if (! isset($argv[3])) {
    exit("Usage: php ".basename(__FILE__)." <NAME> <TYPE> <PRICE>\n");
}
try {
    $product = new Product($argv[1], $argv[2], $argv[3]);
    $product->computeTVA();
    echo " OK TVA calculated\n";
} catch (Exception | TypeError $e) {
    echo " KO TVA not calculated ", $e->getMessage(),"\n";
}