LOG_CS=build/logs/checkstyle.xml
LOG_MD=build/logs/pmd.xml

all: clean tests_unitaires tests_statiques

clean:
	@rm -rf build/logs
	@rm -rf build/coverage
	@rm -rf build/test-results
	@mkdir -p build/logs
	@mkdir -p build/coverage

tests_unitaires:
	@phpunit

tests_statiques:
	@phpcs src/ --standard=psr2 --report=checkstyle --report-file=$(LOG_CS) || exit 0
	@phpmd src/ xml cleancode,codesize,design,naming,unusedcode --reportfile $(LOG_MD) || exit 0